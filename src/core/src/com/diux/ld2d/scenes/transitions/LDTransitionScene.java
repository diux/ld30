package com.diux.ld2d.scenes.transitions;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.diux.ld2d.scenes.LDScene;

public abstract class LDTransitionScene extends LDScene
{
    private boolean complete;
    private boolean started;
    private LDTransitionSceneCompletable delegate;

    protected LDScene inScene;
    protected LDScene outScene;

    protected float duration;

    public LDTransitionScene(Viewport viewport, float duration)
    {
        super(viewport);

        this.duration = duration;
        complete = false;
        started = false;
    }

    public void setDelegate(LDTransitionSceneCompletable delegate)
    {
        this.delegate = delegate;
    }

    //Start the transition
    public void start(LDScene inScene, LDScene outScene)
    {
        this.inScene = inScene;
        this.outScene = outScene;

        this.start();

        started = true;
    }

    //Override in subclass to start the transition animation
    protected void start()
    {

    }

    //Called after the completion of the transition
    //Override and use this method to restart the scenes to their original state
    protected void reset()
    {

    }

    protected void complete()
    {
        reset();
        complete = true;

        //Let the LDSceneManager know that we finished the transition
        delegate.transitionCompleted();
    }

    /**
     * Draw and update both scenes as we animate them.
     */
    public void render (float delta)
    {
        if (!started)
            return;

        if (!complete)
        {
            outScene.act(delta);
            outScene.draw();
        }

        inScene.act(delta);
        inScene.draw();
    }

    @Override
    public void dispose ()
    {
        super.dispose();
    }
}
