package com.diux.ld2d.scenes.transitions;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelegateAction;
import com.badlogic.gdx.utils.viewport.Viewport;

public class LDFadeOutTransition extends LDTransitionScene
{
    //TODO:(Jesus) Add an object pool for transition instances
    public LDFadeOutTransition(Viewport viewport, float duration)
    {
        super(viewport, duration);
    }

    @Override
    protected void start()
    {
        super.start();

        //Execute a fade in on the incoming scene + its children
        inSceneFadeIn();
    }

    private void inSceneFadeIn()
    {
        Group inSceneRoot = inScene.getRoot();
        inSceneRoot.setColor(inSceneRoot.getColor().r, inSceneRoot.getColor().g, inSceneRoot.getColor().b, 0.0f);

        for  (Actor actor : inSceneRoot.getChildren())
        {
            actor.setColor(inSceneRoot.getColor().r, inSceneRoot.getColor().g, inSceneRoot.getColor().b, 0.0f);
        }

        DelegateAction transitionFinishedAction = new DelegateAction()
        {
            @Override
            protected boolean delegate(float delta)
            {
                complete();
                return true;
            }
        };

        //Sequence animation: Fade In + Complete call
        inSceneRoot.addAction(Actions.sequence(Actions.fadeIn(duration), transitionFinishedAction));

        for  (Actor actor : inSceneRoot.getChildren())
        {
            actor.addAction(Actions.fadeIn(duration));
        }
    }

    @Override
    protected void reset()
    {
        //We are only modifying the outScene. No need for restoring its values.
        super.reset();
    }

    @Override
    public void dispose ()
    {
        super.dispose();
    }
}
