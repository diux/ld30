package com.diux.ld2d.scenes.transitions;

public interface LDTransitionSceneCompletable
{
    /** Called when the transition is over. */
    public void transitionCompleted ();
}