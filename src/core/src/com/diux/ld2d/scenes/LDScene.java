package com.diux.ld2d.scenes;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

public abstract class LDScene extends Stage implements Screen
{
    public LDScene(Viewport viewport)
    {
        super(viewport);
        this.init();
    }

    /** Called at the creation of the LDScene */
    public void init ()
    {

    }

    /** Called when the screen should render itself.
     * @param delta The time in seconds since the last render. */
    public void render (float delta)
    {
        this.act(delta);
        this.draw();
    }

    /** @see com.badlogic.gdx.ApplicationListener#resize(int, int) */
    public void resize (int width, int height)
    {
        this.getViewport().update(width, height, true);
    }

    /** Called when this screen will become the current screen for a LDSceneManager.
     *  This is called i between the transition animation between two LDScene's.
     * */
    public void willShow ()
    {

    }

    /** Called when this screen becomes the current screen for a LDSceneManager. */
    public void show ()
    {

    }

    /** Called when this screen will no longer be the current screen for a LDSceneManager.
     *  This is called in between the transition animation between two LDScene's.
     * */
    public void willHide ()
    {

    }

    /** Called when this screen is no longer the current screen for a LDSceneManager. */
    public void hide ()
    {

    }

    /** @see com.badlogic.gdx.ApplicationListener#pause() */
    public void pause ()
    {

    }

    /** @see com.badlogic.gdx.ApplicationListener#resume() */
    public void resume ()
    {

    }

    /** Called when this screen should release all resources. */
    public void dispose ()
    {

    }
}
