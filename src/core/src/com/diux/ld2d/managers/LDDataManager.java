package com.diux.ld2d.managers;

public class LDDataManager
{
    private static LDDataManager s_instance = null;

    private LDDataManager()
    {
    }

    private static LDDataManager sharedInstance()
    {
        if(s_instance == null)
        {
            s_instance = new LDDataManager();
        }

        return s_instance;
    }

    //Implement methods to read/write save data (score/etc).
}
