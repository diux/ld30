package com.diux.ld2d.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;
import java.util.Map;

public class LDSoundManager
{
    private static LDSoundManager s_instance = null;

    private Map<String,Sound> playingSounds;
    private String currentMusicName;
    private Music currentMusic;

    //TODO (Jesus)::Find how and when to dispose the loaded sounds if they are no longer required

    private LDSoundManager()
    {
        playingSounds = new HashMap<String,Sound>();
        currentMusicName = "";
        currentMusic = null;
    }

    private static LDSoundManager sharedInstance()
    {
        if(s_instance == null)
        {
            s_instance = new LDSoundManager();
        }

        return s_instance;
    }

    public static void clearSounds()
    {
        Map<String,Sound> sounds = sharedInstance().playingSounds;

        for(Sound sound : sounds.values())
        {
            try
            {
                sound.stop();
                sound.dispose();
            }
            catch(Exception e){}
        }
        sounds.clear();
    }

    public static Sound playSound(String soundName)
    {
        Sound sound;

        if(!sharedInstance().playingSounds.containsKey(soundName))
        {
            sound = Gdx.audio.newSound(Gdx.files.internal("sounds/" + soundName + ".wav"));
            sharedInstance().playingSounds.put(soundName, sound);
        }
        else
        {
            sound = sharedInstance().playingSounds.get(soundName);
        }

        sound.play();

        return sound;
    }

    public static void stopSound(String soundName)
    {
        if(sharedInstance().playingSounds.containsKey(soundName))
        {
            sharedInstance().playingSounds.get(soundName).stop();
        }
    }

    public static Music playMusic(String musicName)
    {
        String currentMusicName = sharedInstance().currentMusicName;
        Music currentMusic = sharedInstance().currentMusic;
        if(currentMusicName.equals(musicName))
        {
            currentMusic.play();
            return currentMusic;
        }
        else
        {
            if(currentMusic!=null)
            {
                currentMusic.stop();
                currentMusic.dispose();
            }

            Music music = Gdx.audio.newMusic(Gdx.files.internal("music/"+musicName+".mp3"));
            sharedInstance().currentMusicName = musicName;
            sharedInstance().currentMusic = music;
            music.setLooping(true);
            music.play();

            return music;
        }
    }

    public static void stopMusic()
    {
        if(sharedInstance().currentMusic != null)
        {
            sharedInstance().currentMusic.stop();
        }
    }
}
