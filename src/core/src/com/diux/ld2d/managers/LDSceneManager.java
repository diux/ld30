package com.diux.ld2d.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.Disposable;
import com.diux.ld2d.scenes.LDScene;
import com.diux.ld2d.scenes.transitions.LDTransitionScene;
import com.diux.ld2d.scenes.transitions.LDTransitionSceneCompletable;


public class LDSceneManager implements Disposable, LDTransitionSceneCompletable
{
    private static LDSceneManager s_instance = null;

    private LDScene scene;

    //Transition scene
    private LDScene inScene;
    private LDScene outScene;

    private LDSceneManager()
    {
        scene = null;
    }

    public static LDSceneManager sharedInstance()
    {
        if(s_instance == null)
        {
            s_instance = new LDSceneManager();
        }

        return s_instance;
    }

    public void pause ()
    {
        if (scene != null) scene.pause();
    }

    public void resume ()
    {
        if (scene != null) scene.resume();
    }

    public void render ()
    {
        if (scene != null)
        {
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            Gdx.gl.glClearColor(0, 0, 0, 1);

            scene.render(Gdx.graphics.getDeltaTime());
        }
    }

    public void resize (int width, int height)
    {
        if (scene != null)
        {
            scene.resize(width, height);
        }
    }

    /**
     * Sets the current LDScene
     */
    public synchronized void setScene (LDScene scene, LDTransitionScene transitionScene)
    {
        //Remove input detection
        Gdx.input.setInputProcessor(null);

        //Keep reference of new scene to set it when the transition is over
        inScene = scene;
        outScene = this.scene;
        outScene.willHide();
        inScene.willShow();

        //Set delegate to the transition scene
        transitionScene.setDelegate(this);
        transitionScene.start(scene, this.scene);

        this.scene = transitionScene;
    }

    /**
     * Sets the current LDScene
     */
    public synchronized void setScene (LDScene scene)
    {
        if (this.scene != null)
        {
            this.scene.willHide();
            this.scene.hide();
            this.scene.dispose();
        }

        this.scene = scene;

        if (this.scene != null)
        {
            this.scene.willShow();
            this.scene.show();
            Gdx.input.setInputProcessor(this.scene);
            this.scene.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
    }

    /**
     * Returns the currently active LDScene
     */
    public LDScene getScene ()
    {
        return scene;
    }

    @Override
    public void dispose ()
    {
        if (scene != null)
        {
            scene.hide();
            scene.dispose();
        }
    }

    @Override
    public void transitionCompleted()
    {
        //Clear the transition scene
        if (this.scene != null)
        {
            this.scene.dispose();
        }

        this.scene = inScene;

        if (this.scene != null)
        {
            this.scene.show();
            Gdx.input.setInputProcessor(this.scene);
            this.scene.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }

        //Finalize the out scene
        outScene.hide();
        outScene.dispose();;

        //Clean up local variables
        outScene = null;
        inScene = null;
    }
}
