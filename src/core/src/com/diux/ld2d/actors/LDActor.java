package com.diux.ld2d.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

//This class extends the basic functionality of an Actor + adds an animated sprite that gets drawn on screen automatically.
public class LDActor extends Actor implements Disposable
{
    //Animated sprite
    private TextureAtlas spriteSheet;
    private Array<Sprite> currentSprites;
    private TextureAtlas.AtlasSprite currentSprite;
    private int currentFrame;
    private float elapsedTime;
    private float frameDelay;
    private boolean shouldLoop;
    private boolean isAnimating;
    private Vector2 anchorPoint;

    public LDActor()
    {
        super();
        currentFrame = 0;
        elapsedTime = 0.0f;
        frameDelay = 0.3f;
        shouldLoop = false;
        anchorPoint = new Vector2(0,0);
    }

    public LDActor(String spriteSheetFileName)
    {
        this();
        spriteSheet = new TextureAtlas("spritesheets/" + spriteSheetFileName);
    }

    public void setAnchorPoint(Vector2 anchorPoint)
    {
        if (anchorPoint.x >= 0 && anchorPoint.x <= 1 && anchorPoint.y >= 0 && anchorPoint.y <= 1)
        {
            this.anchorPoint = anchorPoint;
        }
    }

    public Vector2 getAnchorPoint()
    {
        return anchorPoint;
    }

    public void setFrame(String frameName)
    {
        this.currentFrame = 0;
        this.elapsedTime = 0.0f;
        currentSprite = (TextureAtlas.AtlasSprite)spriteSheet.createSprite(frameName, 0);

        if (currentSprite != null)
        {
            currentSprites = new Array<Sprite>();
            currentSprites.add(currentSprite);
            this.setSize(currentSprite.getWidth(), currentSprite.getHeight());
        }
        else
        {
            Gdx.app.log("LDActor", "Error::LDActor The frameName " + frameName + "doesn't exists");
        }
    }

    public void setColor (Color color)
    {
        super.setColor(color);
    }

    public void runAnimation(String animationName, float frameDelay, boolean shouldLoop)
    {
        this.frameDelay = frameDelay;
        this.shouldLoop = shouldLoop;
        this.currentFrame = 0;
        this.elapsedTime = 0.0f;
        currentSprites = spriteSheet.createSprites(animationName);
        currentSprite = (TextureAtlas.AtlasSprite) currentSprites.get(0);

        if (currentSprites != null && currentSprite != null)
        {
            this.setSize(currentSprite.getWidth(), currentSprite.getHeight());
            isAnimating = true;
        }
        else
        {
            Gdx.app.log("LDActor", "Error::LDActor The animationName " + animationName + "doesn't exists");
        }
    }

    @Override
    public void draw (Batch batch, float parentAlpha)
    {
        super.draw(batch, parentAlpha);

        if (currentSprites != null)
        {
            final TextureAtlas.AtlasSprite currentSprite = (TextureAtlas.AtlasSprite) currentSprites.get(currentFrame);
            float x = getX() - (anchorPoint.x * getWidth());
            float y = getY() - (anchorPoint.y * getWidth());
            currentSprite.setX(x);
            currentSprite.setY(y);
            currentSprite.setScale(getScaleX(), getScaleY());
            currentSprite.setSize(getWidth(), getHeight());
            currentSprite.setRotation(getRotation());
            currentSprite.setColor(getColor());
            currentSprite.draw(batch);
        }
    }

    @Override
    public void act (float delta)
    {
        super.act(delta);

        if (isAnimating)
        {
            //Update current sprite frame
            elapsedTime += delta;

            if (elapsedTime >= frameDelay)
            {
                elapsedTime -= frameDelay;
                if (currentFrame + 1 == currentSprites.size) {
                    if (shouldLoop)
                    {
                        currentFrame = 0;
                    }
                    else
                    {
                        isAnimating = false;
                        //Call animation finished callback (if needed)
                    }
                } else {
                    currentFrame++;
                }
            }
        }
    }

    // Updated drawDebugBounds to include the anchor point
    // TODO (Jesus): Reimplement anchorPoint to modify the origin coordinates instead of overriding these methods
    @Override
    protected void drawDebugBounds (ShapeRenderer shapes)
    {
        if (!getDebug()) return;

        float x = getX() - (anchorPoint.x * getWidth());
        float y = getY() - (anchorPoint.y * getHeight());
        float originX = getOriginX() + (anchorPoint.x * getWidth());
        float originY = getOriginY() + (anchorPoint.y * getHeight());

        shapes.set(ShapeRenderer.ShapeType.Line);
        shapes.setColor(getStage().getDebugColor());
        shapes.rect(x, y, originX, originY, getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }

    // Updated hit to include the anchor point
    // TODO (Jesus): Test if this is working properly
    public Actor hit (float x, float y, boolean touchable)
    {
        x = getX() + (anchorPoint.x * getWidth());
        y = getY() + (anchorPoint.y * getHeight());

        return super.hit(x, y, touchable);
    }

    @Override
    public void dispose()
    {
        if (spriteSheet != null)
        {
            spriteSheet.dispose();
        }
    }
}
