package com.diux.ld30;

import com.badlogic.gdx.ApplicationListener;
import com.diux.ld2d.managers.LDSceneManager;
import com.diux.ld30.scenes.TestScene;

public class LD30DiuxGame implements ApplicationListener
{
    private LDSceneManager sceneManager;

    @Override
    public void create ()
    {
        sceneManager = LDSceneManager.sharedInstance();
        sceneManager.setScene(new TestScene());
    }

    @Override
    public void resize(int width, int height)
    {
        sceneManager.resize(width, height);
    }

    @Override
    public void render()
    {
        sceneManager.render();
    }

    @Override
    public void pause()
    {
        sceneManager.pause();
    }

    @Override
    public void resume()
    {
        sceneManager.resume();
    }

    @Override
    public void dispose()
    {
        sceneManager.dispose();
    }
}
