package com.diux.ld30.scenes;

import com.badlogic.gdx.utils.viewport.FitViewport;
import com.diux.ld2d.managers.LDSceneManager;
import com.diux.ld2d.scenes.LDScene;
import com.diux.ld30.defines.Defines;

public class TestScene extends LDScene
{
    private LDSceneManager sceneManager;

    public TestScene()
    {
		super(new FitViewport(Defines.VIRTUAL_SCREEN_WIDTH,Defines.VIRTUAL_SCREEN_HEIGHT));
    }

    public void init ()
    {
        sceneManager = LDSceneManager.sharedInstance();
    }

    @Override
    public void dispose ()
    {
        super.dispose();
    }
}
