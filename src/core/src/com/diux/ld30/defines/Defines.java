package com.diux.ld30.defines;

/*
 * Defines
 * This class defines all the constants and utilities that all required across all the other SM classes.
 */
public class Defines
{
    //Virtual screen size (iPhone 5 resolution)
    public final static int VIRTUAL_SCREEN_WIDTH            = 640;
    public final static int VIRTUAL_SCREEN_HEIGHT           = 1136;

    //Debug
    public final static boolean SM_ENABLE_DEBUG_DRAWING     = true;
}
